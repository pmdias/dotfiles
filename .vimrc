""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => PEDRO DIAS - 2014 (last update 2015-10-26)
" => .vimrc
""""""""""""""""""""""""""""""""""""""""""""""""""""""""
execute pathogen#infect()

""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Tabs and indents
"
" override with after/ftplugin for specific file types
""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set tabstop=4
set shiftwidth=4
set expandtab
set smarttab
set autoindent

""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Vim interface customization
""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set showcmd
set number
set ruler
set mouse=a
set showmatch
set so=5

set background=dark
colorscheme desert

""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Search and highlighting
""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set hlsearch
set incsearch
set ignorecase
set smartcase

""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Vim general utilities
""""""""""""""""""""""""""""""""""""""""""""""""""""""""
filetype plugin on
filetype indent on
set scrolloff=1
syntax on

set magic
set showmode
set noswapfile
set nowb
set nobackup
set encoding=utf8
set wildignore=*.o,*~,*.pyc
set backspace=eol,start,indent
set lazyredraw
set noerrorbells
set novisualbell

let mapleader=","
let g:mapleader=","


""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Mappings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""
vnoremap > >gv
vnoremap < <gv

" Treat wrapped lines as normal lines
noremap j gj
noremap k gk

" Paste toggle allows to paste from outside buffers preserving indent
set pastetoggle=<F2>

" Save a file using sudo without explicit call to sudo at start of vim
cmap w!! w !sudo tee % >/dev/null

" Map space to search
map <space> /
map <c-space> ?

" Disable highlight when <leader><cr> is pressed
map <silent> <leader><cr> :noh<cr>

" Mappings to better manage tabs
map <leader>tn :tabnew<cr>
map <leader>to :tabonly<cr>
map <leader>tc :tabclose<cr>
map <leader>tm :tabmove

" Quick spelling checks
map <leader>sp :setlocal spell!<cr>

" Quick jump to shell
map <leader>ss :shell<cr>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Plugins (comment or uncomment as needed)
""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" -> TagBar
" nmap <F8> :TagbarToggle<CR>

" -> Powerline
" set rtp+=/usr/local/lib/python2.7/dist-packages/powerline/bindings/vim/
" set laststatus=2
" set t_Co=256

""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Functions and macros
""""""""""""""""""""""""""""""""""""""""""""""""""""""""
func! DeleteTrailingWS()
  exe "normal mz"
  %s/\s\+$//ge
  exe "normal `z"
endfunc
autocmd BufWrite *.py :call DeleteTrailingWS()
